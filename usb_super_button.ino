#include "Keyboard.h"

// Number of buttons
#define N_BUTTON 2

// Maximum number of step for each sequence
#define MAX_STEP 100

// Global Configuration
const int buttonSafeTime = 300;   // Time allowed between two press
                                  // (in 10's of millisecond)
const int keyPressTime = 30;      // Time, in 10's of millisecond, keyboard keys are
                                  // kept pressed

// Button physical connection on Arduino
const int buttonPin[N_BUTTON] = {4, 6};

// Possible action
const int STOP        = 0;
const int WAIT_BUTTON = 1;
const int WAIT_SECOND = 2;
const int PRESS_KEY   = 3;

// Sequence to run for each button
const struct sequence_step {
  int type;
  int param;
} sequence[N_BUTTON][MAX_STEP] = {
  {
    {WAIT_BUTTON, 0  },
    {PRESS_KEY,   'a'},
    {WAIT_SECOND,  1 },
    {PRESS_KEY,   's'},
    {WAIT_BUTTON, 0  },
    {PRESS_KEY,   'a'},
    {WAIT_SECOND,  1 },
    {PRESS_KEY,   's'},
    {WAIT_SECOND, 20 },
    {PRESS_KEY,   '!'},
    {STOP,        0  },
  },
  {
    {WAIT_BUTTON, 0  },
    {PRESS_KEY,   'v'},
    {WAIT_SECOND,  1 },
    {PRESS_KEY,   'e'},
    {WAIT_BUTTON, 0  },
    {PRESS_KEY,   'v'},
    {WAIT_SECOND,  1 },
    {PRESS_KEY,   'e'},
    {WAIT_SECOND, 20 },
    {PRESS_KEY,   '-'},
    {STOP,        0  },
  }
};


struct button_state_s {
  int current_step;
  unsigned long step_time = 0;
  int press_time;

  bool buttonPressed;

  unsigned long timeSinceLastPress;
  int previousButtonState;
} state[N_BUTTON];


void setup() {
  for (int i=0; i<N_BUTTON; i++) {
    // make the pushButton pin an input:
    pinMode(buttonPin[i], INPUT_PULLUP);

    state[i].press_time = 101;
    state[i].timeSinceLastPress = buttonSafeTime;
    state[i].previousButtonState = HIGH;
  }
  
  // initialize control over the keyboard:
  Keyboard.begin();

  // Delay to wait for button line to stabilize
  delay(1000);
}

void loop() {
  for (int i=0; i<N_BUTTON; i++) {
    processButton(i);
  
    // Release the keyboard keys 100ms after pressing them
    if (state[i].press_time == keyPressTime) {
      Keyboard.releaseAll();
    }
    if (state[i].press_time <= keyPressTime) {
      state[i].press_time++;
    }
  
    if (sequence[i][state[i].current_step].type != STOP) {
      switch(sequence[i][state[i].current_step].type) {
        case WAIT_BUTTON:
          if (state[i].buttonPressed) {
            state[i].current_step += 1;
            state[i].step_time = 0;
          }
          break;
        case WAIT_SECOND:
          state[i].step_time++;
          if (state[i].step_time > (sequence[i][state[i].current_step].param*100)) {
            state[i].current_step += 1;
            state[i].step_time = 0;
          }
          break;
        case PRESS_KEY:
          Keyboard.press(sequence[i][state[i].current_step].param);
          state[i].press_time = 0;
          state[i].current_step += 1;
          state[i].step_time = 0;
          break;
      }
    }
  }

  // One sequence step per 10ms
  delay(10);
}



void processButton(int i) {
  if (state[i].timeSinceLastPress <= buttonSafeTime) {
    state[i].timeSinceLastPress += 1;
  }
  
  // read the pushbutton:
  int buttonState = digitalRead(buttonPin[i]);
  // if the button state has changed,
  if ((buttonState != state[i].previousButtonState) &&
      (state[i].timeSinceLastPress > buttonSafeTime)) {
      state[i].buttonPressed = true;
      state[i].timeSinceLastPress = 0;
  } else {
    state[i].buttonPressed = false;
  }
  
  // save the current button state for comparison next time:
  state[i].previousButtonState = buttonState;
}

